const express = require('express');//llamado a libreria
const bodyParser = require('body-parser');//llamado a libreria
// Configuring the database
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');

mongoose.Promise = global.Promise;

// Connecting to the database por la clase connect
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("EXITO! Successfully connected to the database");    
}).catch(err => {
    console.log('ERROR Could not connect to the database. Exiting now...', err);
    process.exit();
});

// create express app
const app = express();//express guardado en variable app

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Bienvenido a la aplicacion."});
});

// ........

// Require Notes routes
require('./app/routes/note.routes.js')(app);

// ........

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
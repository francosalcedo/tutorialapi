module.exports = (app) => {
    const notes = require('../controllers/note.controller.js');

    // Create a new Note
    app.post('/notes', notes.create);//va a la ruta localhost../notes

    // Retrieve all Notes
    app.get('/notes', notes.findAll);//recuperar,traer todas las notas

    // Retrieve a single Note with noteId
    app.get('/notes/:noteId', notes.findOne);//recuperar,traer una sola

    // Update a Note with noteId
    app.put('/notes/:noteId', notes.update);

    // Delete a Note with noteId
    app.delete('/notes/:noteId', notes.delete);//borrar
}